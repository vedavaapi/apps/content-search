from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    long_description = ''

setup(
    name='content_search',
    version='0.0.1',
    packages=['vedavaapi', 'vedavaapi.content_search'],
    author='vedavaapi',
    description='Vedavaapi full text search service',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['requests', 'celery[redis]', 'elasticsearch', 'tqdm', 'vedavaapi-client'],
    classifiers=(
            "Programming Language :: Python :: 3",
            "Operating System :: OS Independent",
    )
)

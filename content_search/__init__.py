import json

import flask
from flask_cors import CORS
from vedavaapi.client import VedavaapiSession

from .lib.importer import ESImportContext
from .lib.oauth import refresh_access_token_if_expired


app = flask.Flask(__name__, instance_relative_config=True)
CORS(app)

try:
    app.config.from_json(filename="config.json")
except FileNotFoundError as e:
    pass

es_conf = app.config.get('ES', {})
es_context = ESImportContext(es_conf.get('hosts', ['localhost']))

vv_conf = app.config.get('VV', {})
vc = VedavaapiSession(vv_conf.get('base_url'))
client_creds_path = vv_conf.get('client_creds_path', '/opt/content_search/client_creds.json')
atr_path = vv_conf.get('atr_path', '/opt/content_search/atr.json')
try:
    atr = json.loads(open(atr_path, 'rb').read().decode('utf-8'))
except:
    atr = {}
atr = refresh_access_token_if_expired(atr, vc, client_creds_path, atr_path)

from .api import api_blueprint_v1
app.register_blueprint(api_blueprint_v1, url_prefix='')

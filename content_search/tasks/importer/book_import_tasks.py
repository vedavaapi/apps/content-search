from celery.exceptions import Ignore
from textract import books

from ... import ESImportContext
from ...celery import celery as celery_app
from ...lib.importer.book_importer import import_book


@celery_app.task(bind=True)
def import_book_task(self, vc, book_id, es_import_context_json):
    es_import_context = ESImportContext.from_json(es_import_context_json)
    import_book(vc, es_import_context, book_id, update_state=self.update_state)

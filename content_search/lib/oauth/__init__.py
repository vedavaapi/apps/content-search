import json
import time

from vedavaapi.client import VedavaapiSession


def _get_new_access_token(vc: VedavaapiSession, clien_creds_path, atr_path):
    client_creds = json.loads(open(clien_creds_path, 'rb').read().decode('utf-8'))
    resp = vc.post('accounts/v1/oauth/token', data={
        "client_id": client_creds.get('client_id'),
        "client_secret": client_creds.get('client_secret'),
        "grant_type": "client_credentials",
    })
    resp.raise_for_status()
    atr = resp.json()
    atr['issued_at'] = time.time()
    open(atr_path, 'wb').write(json.dumps(atr).encode('utf-8'))
    return atr


def refresh_access_token_if_expired(atr, vc, client_creds_path, atr_path):
    #  print(atr.get('issued_at', 0), atr.get('expires_in', 0), time.time())
    if (not atr.get('access_token', None)) or (atr.get('issued_at', 0) + (atr.get('expires_in', 0) - 3600) < time.time()):
        #  print('is expired')
        atr = _get_new_access_token(vc, client_creds_path, atr_path)
    return atr

import flask_restx
from flask import Blueprint


api_blueprint_v1 = Blueprint('content_search_api' + '_v1', __name__)

api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Content search',
    doc='/'
)

from . import importer_ns
from . import search_ns

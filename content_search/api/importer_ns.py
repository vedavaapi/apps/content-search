import json

import flask_restx

from textract.tasks import celery_helper

from .. import es_context, vc, client_creds_path, atr_path, atr
from ..lib.oauth import refresh_access_token_if_expired
from ..tasks.importer.book_import_tasks import import_book_task
from . import api


importer_ns = api.namespace('importer', path='/importer')


@importer_ns.route('/books')
class BooksImporter(flask_restx.Resource):

    post_parser = importer_ns.parser()
    post_parser.add_argument('book_id', type=str, location='form', required=True)
    post_parser.add_argument('access_token', type=str, location='form', required=True)
    post_parser.add_argument('upsert', type=str, location='form', choices=('true', 'false'), default='false')

    @importer_ns.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()
        book_id = args.get('book_id')
        # upsert = json.loads(args.get('upsert', 'false'))

        atr.update(refresh_access_token_if_expired(atr, vc, client_creds_path, atr_path))
        vc.access_token = atr.get('access_token')  # TODO ClientCredentials grant proper manager

        task = import_book_task.delay(vc, book_id, es_context.to_json())
        task_status_url = api.url_for(CeleryTask, task_id=task.id, _external=True)
        return {"status_url": task_status_url, "task_id": task.id}, 202, {
            'Location': task_status_url, 'Access-Control-Expose-Headers': 'Location'
        }

@importer_ns.route('/tasks/<task_id>')
class CeleryTask(flask_restx.Resource):

    def get(self, task_id):
        from ..celery import celery as celery_app
        return celery_helper.get_task_status(celery_app, task_id)

    def delete(self, task_id):
        from ..celery import celery as celery_app
        celery_helper.terminate_task(celery_app, task_id)
        return {"task_id": task_id, "success": True}

from textract.tasks.celery_helper import make_celery
celery = make_celery('content_search.celery:celery', ['content_search.tasks.importer.book_import_tasks'])